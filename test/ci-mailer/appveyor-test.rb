# Copyright (C) 2018-2019  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CIMailerAppVeyorTest < Test::Unit::TestCase
  include Helper

  def setup
    options = YAML.load_file(fixture_path("config-multi-site.yaml"))
    @boundary = "boundary"
    options["boundary"] = @boundary
    @ci_mailer_inputs = []
    options["ci_mailer_stub"] = lambda do |*inputs|
      @ci_mailer_inputs << inputs
    end
    @options = WebhookMailer::Options.new(options)
  end

  def test_passed
    raw_payload = {
      "eventName" => "build_success",
      "eventData" => {
        "passed" => true,
        "started" => "8/15/2018 9:54 AM",
        "finished" => "8/15/2018 11:01 AM",
        "repositoryName" => "pgroonga/pgroonga",
        "branch" => "master",
        "commitId"=>"e7e3f4d739",
        "commitAuthor" => "Kouhei Sutou",
        "commitDate" => "8/15/2018 7:52 AM",
        "commitMessage" => "pg11 jsonb: follow \"P\" adding change",
        "buildUrl" => "https://ci.appveyor.com/project/groonga/pgroonga/build/1099",
        "commitUrl" => "https://github.com/pgroonga/pgroonga/commit/e7e3f4d739",
        "jobs" => [
          {
            "id" => "e6gp6i2c6adaawso",
            "name" => "Environment: VS_VERSION=12, ARCH=amd64, POSTGRESQL_VERSION=10.5-1, WAL_SUPPORTED=yes",
            "passed" => true,
            "status" => "Success",
            "started" => "8/15/2018 10:41 AM",
            "finished" => "8/15/2018 11:01 AM",
            "duration" => "00:19:43.9607566",
          },
          {
            "id" => "re4ir5wnbl0se5y7",
            "name" => "Environment: VS_VERSION=12, ARCH=amd64, POSTGRESQL_VERSION=9.6.10-1, WAL_SUPPORTED=yes",
            "passed" => true,
            "status" => "Success",
            "started" => "8/15/2018 10:18 AM",
            "finished" => "8/15/2018 10:41 AM",
            "duration" => "00:23:30.9956929",
          },
        ],
      },
    }
    payload = WebhookMailer::Payload.new(raw_payload)
    mailer = WebhookMailer::CIMailer.new({
                                           "status" => "failed",
                                           "label" => "failed",
                                         },
                                         payload,
                                         @options)
    mailer.send_email
    mail = <<-MAIL.gsub(/\n/, "\r\n")
X-Mailer: WebhookMailer/#{WebhookMailer::VERSION}
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary=#{@boundary}
From: AppVeyor <sender@example.com>
Sender: sender@example.com
To: global-to@example.com
Subject: pgroonga/pgroonga success [master] e7e3f4d7
Date: Wed, 15 Aug 2018 11:01:00 -0000

--#{@boundary}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

AppVeyor: failed -> success
  https://ci.appveyor.com/project/groonga/pgroonga/build/1099

Builds:
  Success: 19m43s:
    https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/e6gp6i2c6adaawso
    Environment:
      VS_VERSION=12
      ARCH=amd64
      POSTGRESQL_VERSION=10.5-1
      WAL_SUPPORTED=yes
  Success: 23m30s:
    https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/re4ir5wnbl0se5y7
    Environment:
      VS_VERSION=12
      ARCH=amd64
      POSTGRESQL_VERSION=9.6.10-1
      WAL_SUPPORTED=yes

Commit author:
  Kouhei Sutou
Commit date:
  2018-08-15T07:52:00Z
Commit ID:
  e7e3f4d739
Commit changes:
  https://github.com/pgroonga/pgroonga/commit/e7e3f4d739
Commit message:
  pg11 jsonb: follow "P" adding change

--#{@boundary}
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      AppVeyor:
      failed &rarr;
      <a href="https://ci.appveyor.com/project/groonga/pgroonga/build/1099">
        <span style="background-color: #a6f3a6; color: #000000">success</span>
      </a>
    </h1>
    <h2>Builds</h2>
    <table>
      <thead>
        <tr>
          <th>Status</th>
          <th>Elapsed</th>
          <th>Environment</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/e6gp6i2c6adaawso"><span style="background-color: #a6f3a6; color: #000000">Success</span></a></td>
          <td>19m43s</td>
          <td>
            <ul>
              <li>VS_VERSION=12</li>
              <li>ARCH=amd64</li>
              <li>POSTGRESQL_VERSION=10.5-1</li>
              <li>WAL_SUPPORTED=yes</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td><a href="https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/re4ir5wnbl0se5y7"><span style="background-color: #a6f3a6; color: #000000">Success</span></a></td>
          <td>23m30s</td>
          <td>
            <ul>
              <li>VS_VERSION=12</li>
              <li>ARCH=amd64</li>
              <li>POSTGRESQL_VERSION=9.6.10-1</li>
              <li>WAL_SUPPORTED=yes</li>
            </ul>
          </td>
        </tr>
      </tbody>
    </table>
    <h2>Commit</h2>
    <dl style="line-height: 1.5; margin-left: 2em">
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Author</dt>
      <dd style="margin-left: 8.5em">Kouhei Sutou</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Date</dt>
      <dd style="margin-left: 8.5em">2018-08-15T07:52:00Z</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">New revision</dt>
      <dd style="margin-left: 8.5em"><a href="https://github.com/pgroonga/pgroonga/commit/e7e3f4d739">e7e3f4d739</a></dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Message</dt>
      <dd style="margin-left: 8.5em"><pre style="border: 1px solid #aaa; font-family: Consolas, Menlo, &quot;Liberation Mono&quot;, Courier, monospace; line-height: 1.2; padding: 0.5em; width: auto">pg11 jsonb: follow &quot;P&quot; adding change</pre></dd>
    </dl>
  </body>
</html>

--#{@boundary}--
    MAIL
    assert_equal([
                   [
                     "localhost",
                     25,
                     "sender@example.com",
                     ["global-to@example.com"],
                     mail,
                   ]
                 ],
                 @ci_mailer_inputs)
  end

  def test_pull_request
    raw_payload = {
      "eventName" => "build_success",
      "eventData" => {
        "passed" => true,
        "started" => "8/15/2018 9:54 AM",
        "finished" => "8/15/2018 11:01 AM",
        "repositoryName" => "pgroonga/pgroonga",
        "branch" => "master",
        "commitId"=>"e7e3f4d739",
        "commitAuthor" => "Kouhei Sutou",
        "commitDate" => "8/15/2018 7:52 AM",
        "commitMessage" => "pg11 jsonb: follow \"P\" adding change",
        "buildUrl" => "https://ci.appveyor.com/project/groonga/pgroonga/build/1099",
        "commitUrl" => "https://github.com/pgroonga/pgroonga/commit/e7e3f4d739",
        "isPullRequest" => true,
        "pullRequestId" => 33,
        "jobs" => [
          {
            "id" => "e6gp6i2c6adaawso",
            "name" => "Environment: VS_VERSION=12, ARCH=amd64, POSTGRESQL_VERSION=10.5-1, WAL_SUPPORTED=yes",
            "passed" => true,
            "status" => "Success",
            "started" => "8/15/2018 10:41 AM",
            "finished" => "8/15/2018 11:01 AM",
            "duration" => "00:19:43.9607566",
          },
          {
            "id" => "re4ir5wnbl0se5y7",
            "name" => "Environment: VS_VERSION=12, ARCH=amd64, POSTGRESQL_VERSION=9.6.10-1, WAL_SUPPORTED=yes",
            "passed" => true,
            "status" => "Success",
            "started" => "8/15/2018 10:18 AM",
            "finished" => "8/15/2018 10:41 AM",
            "duration" => "00:23:30.9956929",
          },
        ],
      },
    }
    payload = WebhookMailer::Payload.new(raw_payload)
    mailer = WebhookMailer::CIMailer.new({
                                           "status" => "failed",
                                           "label" => "failed",
                                         },
                                         payload,
                                         @options)
    mailer.send_email
    mail = <<-MAIL.gsub(/\n/, "\r\n")
X-Mailer: WebhookMailer/#{WebhookMailer::VERSION}
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary=#{@boundary}
From: AppVeyor <sender@example.com>
Sender: sender@example.com
To: global-to@example.com
Subject: pgroonga/pgroonga success [PR#33] e7e3f4d7
Date: Wed, 15 Aug 2018 11:01:00 -0000

--#{@boundary}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

AppVeyor: failed -> success
  https://ci.appveyor.com/project/groonga/pgroonga/build/1099

Builds:
  Success: 19m43s:
    https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/e6gp6i2c6adaawso
    Environment:
      VS_VERSION=12
      ARCH=amd64
      POSTGRESQL_VERSION=10.5-1
      WAL_SUPPORTED=yes
  Success: 23m30s:
    https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/re4ir5wnbl0se5y7
    Environment:
      VS_VERSION=12
      ARCH=amd64
      POSTGRESQL_VERSION=9.6.10-1
      WAL_SUPPORTED=yes

Commit author:
  Kouhei Sutou
Commit date:
  2018-08-15T07:52:00Z
Commit ID:
  e7e3f4d739
Commit changes:
  https://github.com/pgroonga/pgroonga/commit/e7e3f4d739
Commit message:
  pg11 jsonb: follow "P" adding change

--#{@boundary}
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      AppVeyor:
      failed &rarr;
      <a href="https://ci.appveyor.com/project/groonga/pgroonga/build/1099">
        <span style="background-color: #a6f3a6; color: #000000">success</span>
      </a>
    </h1>
    <h2>Builds</h2>
    <table>
      <thead>
        <tr>
          <th>Status</th>
          <th>Elapsed</th>
          <th>Environment</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/e6gp6i2c6adaawso"><span style="background-color: #a6f3a6; color: #000000">Success</span></a></td>
          <td>19m43s</td>
          <td>
            <ul>
              <li>VS_VERSION=12</li>
              <li>ARCH=amd64</li>
              <li>POSTGRESQL_VERSION=10.5-1</li>
              <li>WAL_SUPPORTED=yes</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td><a href="https://ci.appveyor.com/project/groonga/pgroonga/build/1099/job/re4ir5wnbl0se5y7"><span style="background-color: #a6f3a6; color: #000000">Success</span></a></td>
          <td>23m30s</td>
          <td>
            <ul>
              <li>VS_VERSION=12</li>
              <li>ARCH=amd64</li>
              <li>POSTGRESQL_VERSION=9.6.10-1</li>
              <li>WAL_SUPPORTED=yes</li>
            </ul>
          </td>
        </tr>
      </tbody>
    </table>
    <h2>Commit</h2>
    <dl style="line-height: 1.5; margin-left: 2em">
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Author</dt>
      <dd style="margin-left: 8.5em">Kouhei Sutou</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Date</dt>
      <dd style="margin-left: 8.5em">2018-08-15T07:52:00Z</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">New revision</dt>
      <dd style="margin-left: 8.5em"><a href="https://github.com/pgroonga/pgroonga/commit/e7e3f4d739">e7e3f4d739</a></dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Message</dt>
      <dd style="margin-left: 8.5em"><pre style="border: 1px solid #aaa; font-family: Consolas, Menlo, &quot;Liberation Mono&quot;, Courier, monospace; line-height: 1.2; padding: 0.5em; width: auto">pg11 jsonb: follow &quot;P&quot; adding change</pre></dd>
    </dl>
  </body>
</html>

--#{@boundary}--
    MAIL
    assert_equal([
                   [
                     "localhost",
                     25,
                     "sender@example.com",
                     ["global-to@example.com"],
                     mail,
                   ]
                 ],
                 @ci_mailer_inputs)
  end
end
