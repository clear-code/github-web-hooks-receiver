# Copyright (C) 2018-2019  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CIMailerGitLabTest < Test::Unit::TestCase
  include Helper

  def setup
    options = YAML.load_file(fixture_path("config-multi-site.yaml"))
    @boundary = "boundary"
    options["boundary"] = @boundary
    @ci_mailer_inputs = []
    options["ci_mailer_stub"] = lambda do |*inputs|
      @ci_mailer_inputs << inputs
    end
    @options = WebhookMailer::Options.new(options)
  end

  def test_success
    raw_payload = {
      "object_kind" => "pipeline",
      "object_attributes" => {
        "id" => 27944099,
        "ref" => "master",
        "sha" => "501d96ff710119db3d863d65e8593117453eb02e",
        "before_sha" => "a8238fa9fa56d3dab53ec4013754f13b853c8b75",
        "status" => "success",
        "detailed_status" => "passed",
        "finished_at" => "2018-08-14 03:12:08 UTC",
      },
      "project" => {
        "web_url" => "https://gitlab.com/clear-code/webhook-mailer",
      },
      "commit" => {
        "id" => "501d96ff710119db3d863d65e8593117453eb02e",
        "timestamp" => "2018-08-13T12:57:13Z",
        "message" => "Fix a bug",
        "url" => "https://gitlab.com/clear-code/webhook-mailer/commit/501d96ff710119db3d863d65e8593117453eb02e",
        "author" => {
          "name" => "Kouhei Sutou",
          "email" => "kou@clear-code.com",
        },
      },
      "builds" => [
        {
          "id" => 1,
          "name" => "test:ruby2.4",
          "status" => "success",
          "started_at" => "2018-08-14 03:05:02 UTC",
          "finished_at" => "2018-08-14 03:08:08 UTC",
        },
        {
          "id" => 2,
          "name" => "test:ruby2.5",
          "status" => "success",
          "started_at" => "2018-08-14 03:07:02 UTC",
          "finished_at" => "2018-08-14 03:07:05 UTC",
        },
      ],
    }
    payload = WebhookMailer::Payload.new(raw_payload)
    mailer = WebhookMailer::CIMailer.new({
                                           "status" => "failed",
                                           "label" => "failed",
                                         },
                                         payload,
                                         @options)
    mailer.send_email
    mail = <<-MAIL.gsub(/\n/, "\r\n")
X-Mailer: WebhookMailer/#{WebhookMailer::VERSION}
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary=#{@boundary}
From: GitLab CI <sender@example.com>
Sender: sender@example.com
To: global-to@example.com
Subject: clear-code/webhook-mailer passed [master] 501d96ff
Date: Tue, 14 Aug 2018 03:12:08 -0000

--#{@boundary}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit

GitLab CI: failed -> passed
  https://gitlab.com/clear-code/webhook-mailer/pipelines/27944099

Builds:
  test:ruby2.4: success: 3m6s:
    https://gitlab.com/clear-code/webhook-mailer/-/jobs/1
  test:ruby2.5: success: 3s:
    https://gitlab.com/clear-code/webhook-mailer/-/jobs/2

Commit author:
  Kouhei Sutou
Commit date:
  2018-08-13T12:57:13Z
Commit ID:
  501d96ff710119db3d863d65e8593117453eb02e
Commit changes:
  https://gitlab.com/clear-code/webhook-mailer/compare/a8238fa9fa56...501d96ff7101
Commit message:
  Fix a bug

--#{@boundary}
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>
      GitLab CI:
      failed &rarr;
      <a href="https://gitlab.com/clear-code/webhook-mailer/pipelines/27944099">
        <span style="background-color: #a6f3a6; color: #000000">passed</span>
      </a>
    </h1>
    <h2>Builds</h2>
    <table>
      <thead>
        <tr>
          <th>Status</th>
          <th>Elapsed</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="https://gitlab.com/clear-code/webhook-mailer/-/jobs/1"><span style="background-color: #a6f3a6; color: #000000">success</span></a></td>
          <td>3m6s</td>
          <td>test:ruby2.4</td>
        </tr>
        <tr>
          <td><a href="https://gitlab.com/clear-code/webhook-mailer/-/jobs/2"><span style="background-color: #a6f3a6; color: #000000">success</span></a></td>
          <td>3s</td>
          <td>test:ruby2.5</td>
        </tr>
      </tbody>
    </table>
    <h2>Commit</h2>
    <dl style="line-height: 1.5; margin-left: 2em">
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Author</dt>
      <dd style="margin-left: 8.5em">Kouhei Sutou</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Date</dt>
      <dd style="margin-left: 8.5em">2018-08-13T12:57:13Z</dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">New revision</dt>
      <dd style="margin-left: 8.5em"><a href="https://gitlab.com/clear-code/webhook-mailer/compare/a8238fa9fa56...501d96ff7101">501d96ff710119db3d863d65e8593117453eb02e</a></dd>
      <dt style="clear: both; float: left; font-weight: bold; width: 8em">Message</dt>
      <dd style="margin-left: 8.5em"><pre style="border: 1px solid #aaa; font-family: Consolas, Menlo, &quot;Liberation Mono&quot;, Courier, monospace; line-height: 1.2; padding: 0.5em; width: auto">Fix a bug</pre></dd>
    </dl>
  </body>
</html>

--#{@boundary}--
    MAIL
    assert_equal([
                   [
                     "localhost",
                     25,
                     "sender@example.com",
                     ["global-to@example.com"],
                     mail,
                   ]
                 ],
                 @ci_mailer_inputs)
  end
end
